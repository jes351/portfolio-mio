import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import About from "./Components/ComponentsCss/About";
import FooterMenu from "./Components/ComponentsCss/Footer/footer";
import MenuBar from "./Components/ComponentsCss/NavBar/menuBar";
import Galeria from "./Components/galeria/galeria";
import Home from "./Components/landing";

function App() {
  return (
    <Router>
      <MenuBar />
      <Routes>
        <Route
          path="/portfolio-mio"
          element={
            <Home
              nombre="Yamil Tauil"
              titulo="BackendDeveloper"
              texto="Javascript,Nodejs,Express, Mongo, SQL, sequelize"
            />
          }
        />
        <Route path="/about" element={<About />}/>
	     
        <Route path="/projects" element={<Galeria />} />
      </Routes>
      <FooterMenu />
    </Router>
  );
}

export default App;
