import React, { useState } from "react";
import "../NavBar/menuBar.css"
import { social } from "./data";
import resume from "../NavBar/resume.pdf"

function FooterMenu() {
  const [showLinks, setShowLinks] = useState(false);

  return (
    <nav>
      <div className="nav-center">
        <div className="nav-header">
          <a href={resume} target="_blank" rel="noreferrer" download>
          <button
            className="button about">
            MY RESUME
          </button>
        </a>
        </div>
        <ul className="social-icons">
          {social.map((social) => {
            const { id, url, text, icon } = social;
            return (
              <li key={id}>
                <a href={url}>{text} {icon}</a>
              </li>
            );
          })}
        </ul>
      </div>
    </nav>
  );
}

export default FooterMenu;
