// import fileDownload from "js-file-download";
// import axios from "axios";<F2>
import resume from "../imagenes/resume.pdf"
import About from "../ComponentsCss/About";

import "./style.css"

function Home(props) {

  return (<>
    <section>
      <div className="container-home">
        <div id="nombre-home" className="home">
          <div className="nombre">
            I'm <span>{props.nombre}</span>
          </div>
          <div className="text-container" id="texto-home">
            {props.titulo}
            <br />
            {props.texto}
            <br />
          </div>
          <a href={resume}  download>
            <button
              className="button">
              MY RESUME
            </button>
          </a>
        </div>
      </div>
    </section>
    <About></About>

  </>
  );
}
export default Home;
