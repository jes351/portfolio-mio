import auto from "./images/youtube.jpg";
import pomodoro from "./images/wallet.jpg";
import track from "./images/tracker.png";
const data = [

  {
    id: 0,
    url: "https://github.com/yamilt351/Social-media",
    img: auto,
    texto: "Social media Backend Aplication",
    descripcion: " like dislike and suscribe to yours favorites channels!",
    parrafo: "bcrypt, Javascript, Nodejs, Mongoose, Express",
  },
  {
    id: 3,
    url: "https://github.com/yamilt351/grupo-6-backend-NodeJs",
    img: pomodoro,
    texto: "Virtual Wallet Rest Api",
    descripcion: "My first teamwork application",
    parrafo: "Javascript,nodejs, express, sequelize, SQL,swagger, jest, bcrypt, JWT",
  },
  {
    id: 6,
    url: "https://github.com/yamilt351/scraper",
    img: track,
    texto: "web Scraping",
    descripcion: "Simple and powerfull web scraper",
    parrafo: "Nodejs, Express, needle, jest, javascript, appcache, cors, valid-url, express-rate-limit",
  },
];
export default data;
